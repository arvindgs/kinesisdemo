/*
 * Copyright (C) 2019-2020 by Nile Global, Inc.
 * All Rights Reserved.
 *
 * This software is an unpublished work and is protected by copyright and
 * trade secret law.  Unauthorized copying, redistribution or other use of
 * this work is prohibited.
 *
 * The above notice of copyright on this source code product does not indicate
 * any actual or intended publication of such source code.
 */
package com.example.kinesisdemo;

import lombok.AllArgsConstructor;

import java.util.Objects;

public class AuditDetailParameter {

    String name;
    String type;
    String value;

    public AuditDetailParameter() {}

    public AuditDetailParameter(String name, String type, String value) {
        this.name = name;
        this.type = type;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuditDetailParameter that = (AuditDetailParameter) o;
        return Objects.equals(getName(), that.getName())
                && Objects.equals(getType(), that.getType())
                && Objects.equals(getValue(), that.getValue());
    }

    @Override
    public String toString() {
        return "AuditDetailParameter{"
                + "name='"
                + name
                + '\''
                + ", type='"
                + type
                + '\''
                + ", value='"
                + value
                + "\'}";
    }
}
