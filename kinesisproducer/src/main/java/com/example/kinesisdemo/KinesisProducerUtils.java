package com.example.kinesisdemo;

/*
 * Copyright (C) 2019 by Nile Global, Inc.
 * All Rights Reserved.
 *
 * This software is an unpublished work and is protected by copyright and
 * trade secret law.  Unauthorized copying, redistribution or other use of
 * this work is prohibited.
 *
 * The above notice of copyright on this source code product does not indicate
 * any actual or intended publication of such source code.
 */

import com.amazonaws.regions.Regions;
import javax.annotation.PostConstruct;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.kinesis.KinesisAsyncClient;
import software.amazon.awssdk.services.kinesis.model.PutRecordRequest;
import software.amazon.kinesis.common.KinesisClientUtil;

@Component
public class KinesisProducerUtils {

    private static final Logger log = LoggerFactory.getLogger(KinesisProducerUtils.class);

    private Regions defaultAWSRegion = Regions.US_WEST_2;

    private KinesisAsyncClient client;

    @PostConstruct
    public void init() {
        client = getAsyncClient();
    }

    KinesisAsyncClient getAsyncClient() {
        Region region = Region.of(ObjectUtils.firstNonNull(defaultAWSRegion.getName()));
        return KinesisClientUtil.createKinesisAsyncClient(
                KinesisAsyncClient.builder().region(region));
    }

    public boolean postMessage(String streamName, String payload) {
        if (client == null) {
            log.info("Payload is {}", payload);
            log.error("Unable to audit information : Invalid client");

            return false;
        }
        return postMessage(client, streamName, payload);
    }

    public boolean postMessage(KinesisAsyncClient kinesis, String streamName, String payload) {
        long createTime = System.currentTimeMillis();

        PutRecordRequest putRecordRequest =
                PutRecordRequest.builder()
                        .streamName(streamName)
                        .data(SdkBytes.fromUtf8String(payload))
                        .partitionKey(String.format("partitionKey-%d", createTime))
                        .build();

        try {
            kinesis.putRecord(putRecordRequest).get();
            log.debug("Successfully put record, partition key");
            return true;
        } catch (Exception e) {
            if (e.getCause()
                    instanceof
                    software.amazon.awssdk.services.kinesis.model.ResourceNotFoundException) {
                log.info("Destination stream: {} does not exist....", streamName);
            } else {
                log.error("Exception while sending data to Kinesis. ", e);
            }
        }
        return false;
    }
}

