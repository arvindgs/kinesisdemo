package com.example.kinesisdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.function.FunctionConfiguration;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@EnableScheduling
@EnableBinding(Source.class)
@SpringBootApplication(exclude = FunctionConfiguration.class)
public class KinesisProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(KinesisProducerApplication.class, args);
	}

	@Autowired
	private Source source;

	@Autowired private AuditClient handler;

	@Scheduled(fixedRate = 2000L)
	public void sendMessage() {
		AuditMessage message = handler.getHelper().createMessage();
		System.out.println("Sending: "+message);
		handler.sendMessage(message);
	}
}
