package com.example.kinesisdemo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
public class AuditMessage {
    private Date time;
    private String user;
    private String tenantId;
    private String sourceIP;
    private String agent;
    private Boolean customerVisible = false;

    private AuditDetail details;

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getSourceIP() {
        return sourceIP;
    }

    public void setSourceIP(String sourceIP) {
        this.sourceIP = sourceIP;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public AuditDetail getDetails() {
        return details;
    }

    public void setDetails(AuditDetail details) {
        this.details = details;
    }

    public Boolean getCustomerVisible() {
        return customerVisible;
    }

    public void setCustomerVisible(Boolean customerVisible) {
        this.customerVisible = customerVisible;
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return "";
        }
    }
}
