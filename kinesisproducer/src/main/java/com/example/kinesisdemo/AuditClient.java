package com.example.kinesisdemo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AuditClient {

    @Autowired AuditHelper helper;

    private ObjectMapper mapper = new ObjectMapper();

    @Autowired private KinesisProducerUtils producerUtils;

    @Value("arvind-test-api-audit")
    private String auditStreamName;

    private static final Logger logger = LogManager.getLogger(AuditClient.class);

    public boolean sendMessage(AuditMessage message) {
        String payload = null;
        try {
            payload = mapper.writeValueAsString(message);
            producerUtils.postMessage(auditStreamName, payload);
            return true;
        } catch (JsonProcessingException e) {
            logger.error("Failed to deserialize audit message ", e);
        } catch (Exception e) {
            logger.error("Failed to post audit message ", e);
        }
        return false;
    }

    public AuditHelper getHelper() {
        return helper;
    }

    void setAuditStreamName(String auditStreamName) {
        this.auditStreamName = auditStreamName;
    }
}
