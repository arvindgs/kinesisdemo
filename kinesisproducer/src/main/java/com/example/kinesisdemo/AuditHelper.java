package com.example.kinesisdemo;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;

@Component
public class AuditHelper {

    private ObjectMapper mapper = new ObjectMapper();
    private static final Logger logger = LogManager.getLogger(AuditHelper.class);

    /**
     * Method to build base Audit message with Authz information
     *
     * @return
     */
    public AuditMessage createMessage() {
        AuditMessage message = new AuditMessage();
        message.setTime((new Date()));

        message.setUser("arvind@nile.com");
        message.setTenantId("7b257102-c976-4487-b06b-e953a5aab7e4");
        message.setSourceIP("1.1.1.1");
        message.setAgent("userAgent");
        message.setDetails(new AuditDetail("98765432","987654321","0987654321","0987654321","0987654321","0987654321","0987654321","0987654321","098765432","0987654321","0987654321","09765432","098765432","098765432","098765432", new ArrayList<>()));
        return message;
    }
}
