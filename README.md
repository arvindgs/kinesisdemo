# To run consumer
1. `mvn package`
2. `java -jar kinesisconsumer/target/kinesisconsumer-0.0.1-SNAPSHOT.jar`

# To run producer
1. `mvn package`
2. `java -jar target/kinesisproducer-0.0.1-SNAPSHOT.jar`
